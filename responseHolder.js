const getSpecificFieldsFromDish = (arr) => {
    return arr.map((dish) =>
      (({ name, ingredients, cooktime }) => ({ name, ingredients, cooktime }))(
        dish
      )
    );
  };

  module.exports = getSpecificFieldsFromDish; 