function differenceTime(asr, maghreb) {
    let asrHour = asr.substring(0, 2);
    let asrMinutes = asr.substring(3, 5);
    let maghrebHour = maghreb.substring(0, 2);
    let maghrebMinutes = maghreb.substring(3, 5);
  
    let diffrence = (maghrebHour - asrHour) * 60;
    if (parseInt(asrMinutes) > parseInt(maghrebMinutes))
      diffrence = diffrence - (asrMinutes - maghrebMinutes);
    else diffrence = diffrence + (maghrebMinutes - asrMinutes);
    return diffrence - 15;
  }

  module.exports = differenceTime; 