const express = require("express");
const req = require("express/lib/request");
const res = require("express/lib/response");
const axios = require("axios");
const app = express();
const dishes = require("./dishes.json");
const differenceTime = require("./difference_holder")
const getSpecificFieldsFromDish = require("./responseHolder")

app.get("/cooktime", (req, res) => {
  var ingredient = req.query.ingredient;

  var dateParam = req.query.date;

  axios
    .get(
      "http://api.aladhan.com/v1/hijriCalendar?latitude=51.508515&longitude=-0.1254872&method=1&month=9&year=1443"
    )
    .then((resp) => {
      resp.data.data.map((date) => {
        if (date.date.hijri.day == dateParam) {
          var result = dishes.filter(function (e, i) {
            return dishes[i].ingredients.includes(ingredient);
          });

          //console.log(resp.data.data[0].date.hijri.date)
          result.map((d) => {
            if (
              d.duration >
              differenceTime(date.timings.Asr, date.timings.Maghrib)
            ) {
              d.cooktime = `${d.duration -
                differenceTime(date.timings.Asr, date.timings.Maghrib)} minutes before Asr`;
            } else {
              d.cooktime = `${differenceTime(date.timings.Asr, date.timings.Maghrib) -
                d.duration} minutes After Asr`;
            }
          });
          res.send(getSpecificFieldsFromDish(result));
        }
      });
    });
});

app.get("/suggest", (req, res) => {
  var arr = []
  dishes.map((dish) => {
    arr.push(dish.duration)
  })
  arr.sort((a, b) => a - b);
  dishes.map((dish) => {
    arr.push(dish.duration)
  })

  var suggest = dishes.filter(function (e, i) {
    return dishes[i].duration== arr[0];
  });

  res.send(suggest)

})

app.listen(3000, () => console.log("listening on port 3000"));


